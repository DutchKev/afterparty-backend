const config= require('../../config/config.json');
const _     = require('lodash');
const push  = require('./push');

var sequelize = require('../index');

exports.loadConversations = function (opt, callback) {

    var lastChecked = new Date(opt.lastChecked || 0);

    this.findConversations({userUID: opt.userUID}, function (conversations) {

        var conversationIdArray = conversations.map(function (c) {
            return c.uid
        });

        sequelize.models.message.findAll({
            where: {
                conversation_uid: {$in: conversationIdArray},
                created_at: {$gt: lastChecked}
            },
            raw: true
        }).then(function (messages) {

            var groupedMessages = [];

            // Group by conversation_id
            for (var i = 0, len = messages.length; i < len; ++i) {
                var m = messages[i];
                var group = _.findWhere(groupedMessages, {conversation_uid: m.conversation_uid});

                m.from_me = (m.sender_id == opt.userUID);

                if (group) {
                    group.messages.push(m);
                } else {
                    groupedMessages.push({
                        conversation_uid: m.conversation_uid,
                        messages: [m],
                        user: _.findWhere(conversations, {uid: m.conversation_uid}).user
                    });
                }
            }

            // Create array of conversation users
            var otherUsers = groupedMessages.map(function (g) {
                return g.user
            });

            // Find the users
            sequelize.models.user.findAll({
                attributes: ['uid', 'username', 'gender'],
                where: {uid: {$in: otherUsers}},
                raw: true
            }).then(function (users) {
                for (var i = 0, len = users.length; i < len; ++i) {
                    _.findWhere(groupedMessages, {user: users[i].uid}).user = users[i]
                }

                callback(groupedMessages);
            });
        })
    });
};

exports.findConversations = function (opt, callback) {

    if (!opt.userUID) {
        throw new Error('findConversation: No userUID is given');
    }

    sequelize.models.conversation.findAll({
        where: {
            $or: [
                {
                    user_uid: {$eq: opt.userUID}
                },
                {
                    user2_uid: {$eq: opt.userUID}
                }
            ]
        },

    }).then(function (conversations) {
        conversations.forEach(function (c) {
            c.user = {
                uid: c.user_uid == opt.userUID ? c.user2_uid : c.user_uid
            };
        });

        typeof callback == 'function' && callback(conversations);
    });
};


exports.addMessage = function reqUser(reqUser, opt, callback) {

    if (!opt.message)
        throw new Error('message is empty');

    if (!reqUser || !reqUser.uid)
        throw new Error('No owner user is given for the new message');

    opt.sender_id = reqUser.uid;
    opt.receiver_id = opt.toUserUID;

    this.findOrCreate(opt, function (conversation) {

        if (!conversation.uid)
            throw new Error('missing conversation_uid');

        sequelize.models.message.create({
            sender_id: reqUser.uid,
            message: opt.message,
            conversation_uid: conversation.uid
        }, {raw: true}).then(function (message) {

            // Update the conversation last_changed field
            conversation.update({updatedAt: new Date()});

            if (typeof callback == 'function') {
                callback(message);
            }
        });

        var pushOptions = {
            toUserUID: '',
            fromUserUID: reqUser.uid,
            title: 'New message',
            message: opt.message
        };

        push(pushOptions).then(function() {

        });
    });
};

exports.findOrCreate = function (opt, callback) {
    console.log(opt);

    sequelize.models.conversation.find({
        where: {
            $or: [
                {
                    $and: [{user_uid: {$eq: opt.sender_id}}, {user2_uid: {$eq: opt.receiver_id}}]
                }, {
                    $and: [{user_uid: {$eq: opt.receiver_id}}, {user2_uid: {$eq: opt.sender_id}}]
                }
            ]
        }
    }, {raw: true}).then(function (conversation) {

        if (conversation)
            callback(conversation);
        else
            sequelize.models.conversation.create({
                user_uid: opt.sender_id,
                user2_uid: opt.receiver_id
            }).then(function (conversation) {
                //console.log(conversation.id);
                callback(conversation);
            })
    });
}