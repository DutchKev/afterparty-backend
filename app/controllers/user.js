'use strict';

const _           = require('lodash');
const User        = require('../index').models.user;

exports.create = function(opt, callback) {
    opt.hashed_password = opt.password;

    User.create(opt).then(function(user) {
        callback(user);
    });
};

exports.update = function(opt, callback) {};

exports.delete = function(opt, callback) {};

exports.load = function(reqUser, opt, callback) {
    User.findById(opt.uid).then(function(user) {
        user = user && user.filter();

        callback(user);
    });
};

exports.list = function(reqUser, opt, callback) {
    (opt || (opt = {}));

    let options = {
        where: {
            uid: {$ne: reqUser.uid} // Exclude self
        },
        offset: parseInt(opt.offset, 10) || 0,
        limit: parseInt(opt.limit, 10) || 40
    };

    if (opt.search)
        options.where.username = {$like: '%' + opt.search + '%'};


    require('../../app/index').models.user.findAll(options).then(function (users) {

        let list = users.map(function(u){return u.filter()});

        callback(list);
    });
};