'use strict';

var _ = require('lodash');

function sendMessageGCM(toUsers, title, message, type) {

    console.log('PUSH GCM - ', message);
    var http = require('http');

    var data = {
        "collapseKey": "applice",
        "delayWhileIdle": true,
        "timeToLive": 3,
        "data": {
            "message": message,
            "title": title
        },

        "registration_ids": toUsers
    };

    var dataString = JSON.stringify(data);
    var headers = {
        'Authorization': 'key=AIzaSyBZTYCMAdYw03lj69eahhdaS7L7kRM8G3I',
        'Content-Type': 'application/json',
        'Content-Length': dataString.length
    };

    var options = {
        host: 'android.googleapis.com',
        port: 80,
        path: '/gcm/send',
        method: 'POST',
        headers: headers
    };

    //Setup the request
    var req = http.request(options, function (res) {
        res.setEncoding('utf-8');

        var responseString = '';

        res.on('data', function (data) {
            responseString += data;
        });

        res.on('end', function () {
            try {
                var resultObject = JSON.parse(responseString);
                console.log(resultObject);
            } catch (error) {
                console.log(responseString)
            }
        });
        //console.log('STATUS: ' + res.statusCode);
        //console.log('HEADERS: ' + JSON.stringify(res.headers));

    });

    req.on('error', function (e) {
        // TODO: handle error.
        console.log('error : ' + e.message + e.code);
    });

    req.write(dataString);
    req.end();
}

function sendMessageSocket() {

}

module.exports = function (opt) {

    var promise = new Promise(function (resolve, reject) {
        console.log('sendGCM:', opt);

        sendMessageGCM([
            "APA91bHeaCYI81C5o5whlur1yW3FNrNN5ySMqBUTOf7lBifq9q-bLVUCGME1kfPUpZ9QtMTXU9N1W7ADigfr8l6O7ttgMTwLyR9WiTFr_oRGCBtCZbTOAQAdSzm189Rnk-RrQd5ylE3TvPhRbCyTvtKmvG_okg6jmA",
            "APA91bGD7l7Ph7ZS5C3YXtnjwPfOAGcLzCUC8sDi-rvc3fZfXwlyNlxPVSw433HHnkvPxEQK1UvQOCgOWXn6IqTrtHFDjogWcwjswC1CjE-qit2arsz-5qbRZwQxGAM72g0DFf55JI18PQ-5YnVvs8b8sPinmkfjJw"
        ], opt.title, opt.message);

        /*
        // Send message to sockets
        // TODO: send email when no socket present?
        var clients = io.sockets.connected;
        for (var key in clients) {
            if (clients.hasOwnProperty(key)) {
                if (clients[key].decoded_token.uid == opt.userUID) {
                    //clients[key].emit('conversation/message', [message]);
                    break;
                }
            }
        }
        */

        console.log('opt:', opt);

        resolve(message);
    });

    return promise;
};