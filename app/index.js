'use strict';

var config      = require('../config/config.json');
var Sequelize   = require('sequelize');

var sequelize = new Sequelize(
    config.db.database,
    config.db.user,
    config.db.password,
    {
        host: config.db.host,
        dialect: 'mysql',

        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);

// Insert models
var User = require('./models/user')(sequelize);
var Message = require('./models/message')(sequelize);

var Conversation =  sequelize.define('conversation', {
    uid: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true
    }
}, {
    underscored: true,
    freezeTableName: true
});

Message.belongsTo(Conversation);
Message.belongsTo(User, {foreignKey: 'sender_id'});

User.hasMany(Conversation);
User.hasMany(Conversation, {as: 'Conversation', foreignKey: 'user2_uid'});

sequelize.sync({force: false});


module.exports = sequelize;