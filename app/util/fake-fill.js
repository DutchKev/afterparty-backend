var _               = require('lodash');
var userModel       = require('../index').models.user;
//var messageController  = require('.../controllers/message');
var conversationController  = require('../controllers/conversation');
var faker           = require('faker');

module.exports = {

    users: function(nrOfUsers) {
        nrOfUsers = nrOfUsers || 1000;

        function insertUser() {
            var from = new Date('1950','01','02');
            var to = new Date('2000','01','02');

            userModel.create({
                username: faker.internet.userName(),
                email: faker.internet.email(),
                date_of_birth: faker.date.between(from, to),
                password: 'faker',
                country: faker.address.country(),
                gender: _.random(0, 1)
            })
        }

        setTimeout(function() {
            while(nrOfUsers--) {

                setTimeout(function() {
                    process.nextTick(insertUser);
                }, 0);

                /*
                userModel.create({
                    username: faker.internet.userName(),
                    email: faker.internet.email(),
                    date_of_birth: faker.date.between(from, to),
                    password: 'faker',
                    country: faker.address.country(),
                    gender: _.random(0, 1) ? 'Male' : 'Female'
                })
                */
            }
        }, 2000);
    },

    messages: function(opt) {
        var nrOfMessages = opt.nrOfMessages || 10;

        var messages = [];

        function insertMessage() {
            var from = opt.from || _.sample(idArray);
            var to = opt.to || 1001;
            var sender = _.sample([from, to]);

            if (from !== to) {
                conversationController.addMessage({
                    message: faker.lorem.sentence(),
                    from: from,
                    to: to,
                    sender_id: sender
                });
            }
        }

        setTimeout(function() {

            userModel.findAll({}).then(function(users) {
                var idArray = users.map(function(u){return u.id});
                var oTimeout = 0;
                while(nrOfMessages--) {

                    var from = opt.from || _.sample(idArray);
                    var to = opt.to || 1001;
                    var sender = _.sample([from, to]);

                    setTimeout(function() {
                        if (from !== to) {
                            conversationController.addMessage({
                                message: faker.lorem.sentence(),
                                from: from,
                                to: to,
                                sender_id: sender
                            });
                        }
                    }, (oTimeout+=100));

                }
            });


/*
            while(nrOfMessages--) {

                messages.push({
                    message: faker.lorem.sentence(),
                    from: from,
                    to: to
                });
            }

            Task.bulkCreate([
                {subject: 'programming', status: 'executing'},
                {subject: 'reading', status: 'executing'},
                {subject: 'programming', status: 'finished'}
            ]);
            */


        }, 2000);
    }
}
