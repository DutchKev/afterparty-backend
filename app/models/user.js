'use strict';

var Sequelize = require('sequelize');

module.exports = function(sequelize) {

    var User = sequelize.define('user', {
        uid: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        email: {
            type: Sequelize.STRING,
            validate:  {
                isEmail: true
                //msg: "Email is invalid"
            }
        },
        password: {
            type: Sequelize.VIRTUAL,
            set: function (val) {
                this.setDataValue('password', val); // Remember to set the data value, otherwise it won't be validated
                this.setDataValue('password_hash', this.salt + val);
            },
            validate: {
                len: {
                    args: 3
                    //msg: "Password must be at least 3 characters in length"
                }
            }
        },
        hashed_password: {type: Sequelize.STRING},
        username: {
            type: Sequelize.STRING,
            validate: {
                len: {
                    args: 2
                    //msg: "Username must be at least 3 characters in length"
                }
            }
        },
        profileIMG: {type: Sequelize.STRING, default: ''},
        gender: {type: Sequelize.STRING, allowNull: false},
        country: {type: Sequelize.STRING, allowNull: false},
        description: {type: Sequelize.TEXT},
        provider: {type: Sequelize.STRING, default: ''},
        salt: {type: Sequelize.STRING, required: true},
        authToken: {type: Sequelize.STRING, default: ''},
        date_of_birth: {type: Sequelize.DATEONLY},
        facebook: {type: Sequelize.STRING, default: ''},
        twitter: {type: Sequelize.STRING, default: ''},
        google: {type: Sequelize.STRING, default: ''}
    }, {
        underscored: true,
        freezeTableName: true,

        instanceMethods: {
            filter: function() {
                return {
                    uid: this.uid,
                    created_at: this.created_at,
                    updated_at: this.updated_at,
                    username: this.username,
                    gender: this.gender,
                    country: this.country,
                    profileIMG: this.profileIMG || ''
                }
            }
        },

        classMethods: {
            authenticate: function() {

            },
            associate: function(models) {

                /*
                User.belongsToMany(models.Happening, {
                    through: 'UserHappening',
                    foreignKey: 'User_id'
                });
                */
            }
        }
    });

    //User.sync();

    return User;
};