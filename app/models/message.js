'use strict';

var Sequelize = require('sequelize');

module.exports = function(sequelize) {

    var Message =  sequelize.define('message', {
        uid: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        message: {type: Sequelize.STRING},
        //sender: {type: Sequelize.STRING},
        read: {type: Sequelize.BOOLEAN}
    }, {
        underscored: true,
        freezeTableName: true,

        instanceMethods: {

        },

        classMethods: {
            associate: function(models) {

                /*
                 User.belongsToMany(models.Happening, {
                 through: 'UserHappening',
                 foreignKey: 'User_id'
                 });
                 */
            }
        }
    });

    return Message;
};