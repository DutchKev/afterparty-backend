'use strict';

const config  = require('../../config/config.json');
const router  = require('express').Router();
const jwt     = require('jsonwebtoken');

router.route('*', function() {
    console.log("ROUTE!")
});


router.route('/auth')

    // Check token
    .get(function(req, res) {
        var token = req.query.token;

        jwt.verify(token, config.auth.secret, function(err, decoded) {
            if (err || !token || !decoded)
                return res.sendStatus(401);

            console.log(decoded);
            return res.sendStatus(202);
        });
    })

    // Login
    .post(function(req, res) {
        let email = req.body.email && req.body.email.toLowerCase(),
            password = req.body.password,
            User = require('../../app/index').models.user;

        User.find({where: {email: email, hashed_password: password}}).then(function(profileData) {
            if (profileData) {
                let filteredData = profileData.filter();

                filteredData.token = jwt.sign(filteredData, config.auth.secret);
                res.json(filteredData);
            } else {
                res.sendStatus(401);
            }
        });
    })

    // Logout
    .delete(function(req, res) {
        console.log('logout');
        res.json({});
    });

module.exports = router;