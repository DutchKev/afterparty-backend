var router          = require('express').Router();
var userController  = require('../../app/controllers/user');

router.route('/register')

    // Create
    .post(function(req, res) {
        userController.create(req.body, function(user, error) {
            // TODO: Implement error
            res.json({error: null, user: user});
        });
    });

module.exports = router;