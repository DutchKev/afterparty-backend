var config  = require('../../config/config.json');

module.exports = function(socket) {

    socket.on('chat/history', function() {
        var Chat = mongoose.model("Chat");

        Chat.find({}, function (err, messages) {
            if (err)
                return console.log(err);

            socket.emit("message", messages);
        });
    });

    socket.on('chat/message', function (msg) {
        var message = {
            username: socket.decoded_token.username,
            gender: socket.decoded_token.gender,
            message: msg
        };
        new (mongoose.model("Chat"))(message).save(function (err, data) {
            if (err) {
                return console.error(err);
            }
            socket.broadcast.emit('message', [message]);
        });
    });

    socket.on('chat/users/next', function () {
        var keys,
            nrOnline = Object.keys(users).length,
            ownID = socket.decoded_token.uid;

        // Min length has to be 2 on live!
        if (nrOnline > 1) {
            keys = _.without(Object.keys(users), socket.decoded_token.currentlyWatching);
        } else {
            keys = Object.keys(users);
        }

        var randomKey = keys[Math.floor(Math.random() * keys.length)];
        var nextUser = users[randomKey];

        users[ownID].currentlyWatching = nextUser.uid;

        socket.emit('chat/users/next', nextUser);
    });
};