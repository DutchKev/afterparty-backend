var config  = require('../../config/config.json');

module.exports = function(socket) {

    socket.on('user/request/stream-token', function () {
        socket.emit('user/request/stream-token', socket.decoded_token.video_url);
    });
};