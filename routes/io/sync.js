var config = require('../../config/config.json');

var conversationController = require('../../app/controllers/conversation');

module.exports = function (socket, io) {

    socket.on('sync', function (params, cb) {

        //console.log('SYNC ME');

        // Prepare app params for controller
        var opt = {
            userUID: socket.decoded_token.uid,
            lastChecked: params.lastChecked || 0,
            knownConversations: params.knownConversations || []
        };

        var returnObj = {
            conversations: [],
            profileViews: []
        };

        console.log(opt);

        conversationController.loadConversations(opt, function(result) {

            returnObj.conversations = result;

            typeof cb == 'function' && cb(returnObj);

            //socket.emit('sync', returnObj);
        });

    });
};