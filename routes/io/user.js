var config = require('../../config/config.json');
var userController = require('../../app/controllers/user');
var _ = require('lodash');

module.exports = function (socket, io) {

    // Get
    socket.on('user', function (params, cb) {
        (params || (params = {}));

        params.id = params.id || socket.decoded_token.id;

        userController.load(params, function(user) {
            cb(user);
        });
    });

    // Update
    socket.on('user/update', function (params) {
    });

    // Delete
    socket.on('user/delete', function (params) {
    });

    // List
    socket.on('user/list', function (params, cb) {
        (params || (params = {}));

        var options = {
            where: {
                uid: {$ne: socket.decoded_token.uid} // Exclude self
            },
            offset: parseInt(params.offset, 10) || 0,
            limit: parseInt(params.limit, 10) || 40
        };

        if (params.search)
            options.where.username = {$like: '%' + params.search + '%'};


        require('../../app/index').models.user.findAll(options).then(function (users) {

            var list = users.map(function(u){return u.filter()});

            cb(list);

            //console.log(list);
            //socket.emit('user/list', list);
        });
    });

    // List
    socket.on('user/activities', function (params) {
        var selfID = socket.decoded_token.uid;

         /*

         var Message = mongoose.model('Message');

         Message.countUnread(selfID, function(count) {
         socket.emit('user/activities', {
         unread: count
         });

         console.log(count);
         });
         */
    });
};