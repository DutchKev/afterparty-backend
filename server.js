'use strict';

const fs        = require('fs');
const config    = require('./config/config.json');
const express   = require('express');
const jwt       = require('express-jwt');
const bodyParser= require("body-parser");
const db        = require('./app/index');

const app       = express();
const server    = require('http').Server(app);
const io        = require('socket.io')(server);

var users = {
    '558ff20c09bc52c805a44b9f': {
        _id: '558ff20c09bc52c805a44b9f',
        username: 'Admin',
        gender: 'Female',
        video_url: 'vod/sample.mp4',
        currentlyWatching: -1
    }
};

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent e.g. in case you use sessions)
    //res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.use(jwt({
    secret: config.auth.secret,
    getToken: (req) => (req.query.token || req.body.token)
}).unless({path: ['/auth', '/register']}));

app.use(function (err, req, res, next) {
    res.status(401).send('invalid token');
});

const routes = {
    'user': 'user#load',
    'user/list': 'user#list',
    'conversation/message': 'conversation#addMessage'
};

// HTTP MIDDLEWARE -> CONTROLLER
app.use(function (req, res) {
    const _ = require('lodash');

    const oURL = req.originalUrl.split('?')[0].replace(/^\/|\/$/g, '');

    if (routes[oURL]) {
        let route = routes[oURL];
        let controller = require('./app/controllers/' + route.split('#')[0]);
        let controllerFunc = route.split('#')[1];

        if (controllerFunc) {
            try {
                let routeParams = _.merge(req.body, req.query);
                delete routeParams.token;

                controller[controllerFunc](req.user, routeParams, function(result) {
                    res.json(result)
                });

            } catch (error) {
                res.sendStatus(404);
                console.log(error);
            }

        }

    } else {
        res.sendStatus(404);
    }

    console.log(oURL);
});

app.use(require('./routes/http/auth'));
app.use(require('./routes/http/register'));


/* websocket */
io.use(require('socketio-jwt').authorize({
    secret: config.auth.secret,
    handshake: true
}));

io.sockets.on('connection', function (socket) {
    console.log('New user connected: ', socket.decoded_token);

    var uid = socket.decoded_token.uid;

    users[uid] = socket.decoded_token;

    socket.decoded_token.video_url = 'live/' + uid;

    var Emitter = require('events').EventEmitter;
    var emit = Emitter.prototype.emit;

    var oOnEvent = socket.onevent;
    socket.onevent = function (packet) {
        var args = packet.data || [];
        oOnEvent.call (this, packet);    // original call
        emit.apply   (this, ["*"].concat(args));      // additional call to catch-all
    };

    // WEBSOCKET MIDDLEWARE -> CONTROLLER
    socket.on('*', function(url, data, callback) {
        var route  = routes[url];

        if (route) {
            let controller = require('./app/controllers/' + route.split('#')[0]);
            let controllerFunc = route.split('#')[1];

            controller[controllerFunc](socket.decoded_token, data, callback);
        }
    });

    socket.on('disconnect', function () {
        console.log(socket.decoded_token.username, 'disconnected');

        users[socket.decoded_token.uid] = null;
        delete users[socket.decoded_token.uid];
    })
});

io.sockets.on('error', function (err) {
    console.error(err);
});

server.listen(config.http.port, function () {
    console.info("Server running on port:" + config.http.port);
});

//require('./app/util/fake-fill').users();
//require('./app/util/fake-fill').messages({from: 2, to: 1});